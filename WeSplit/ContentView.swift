//
//  ContentView.swift
//  WeSplit
//
//  Created by Vrushali Kulkarni on 12/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import SwiftUI

struct ContentView: View {

    @State private var checkAmount = ""
    @State private var numberOfPeople = ""
    @State private var tipPercentage = 2

    private let tipPercentages = [10, 15, 20, 25, 0]

    private var totalPerPerson: Double {

        let orderAmount = Double(checkAmount) ?? 0
        let tipSelection = Double(tipPercentages[tipPercentage])
        guard var totalPeople = Double(numberOfPeople) else { return 0 }
        totalPeople += 2

        let tipValue = orderAmount * tipSelection/100
        let grandTotal = orderAmount + tipValue
        let amountPerPerson = grandTotal/totalPeople
        return amountPerPerson
    }

    private var totalAmount: Double {

        let orderAmount = Double(checkAmount) ?? 0
        let tipSelection = Double(tipPercentages[tipPercentage])

        let tipValue = orderAmount * tipSelection/100
        let grandTotal = orderAmount + tipValue
        return grandTotal
    }

    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Amount", text: $checkAmount)
                        .keyboardType(.decimalPad)

//                    Picker("Number of People", selection: $numberOfPeople) {
//                        ForEach(2..<100) {
//                            Text("\($0) people")
//                        }
//                    }
                    TextField("Number of people", text: $numberOfPeople)
                        .keyboardType(.numberPad)
                }

                Section(header: Text("How much tip you want to leave?")) {
                    Picker("Tip Percentage", selection: $tipPercentage) {
                        ForEach(0..<tipPercentages.count) {
                            Text("\(self.tipPercentages[$0])%")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }

                Section(header: Text("Amount per person")) {
                    Text("$\(totalPerPerson, specifier: "%.2f")")
                }

                Section(header: Text("Total amount for the check")) {
                    Text("\(totalAmount)")

                }
            }
        .navigationBarTitle("WeSplit")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
